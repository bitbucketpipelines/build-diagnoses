# Build diagnoses registry

## What are build diagnoses?

This repository contains a curated set of build faiulre diagnoses hosted on Bitbucket and displayed in Bitbucket Pipelines UI as suggested fixes in Build Doctor.

## Diagnoses structure

To add or modify a diagnosis, find or create a new file `.yml` file in diagnoses folder.

The schema for a diagnosis is:

```
key: 'build_doctor.missing_docker_dependency' # analytic event that fires when the diagnosis matches
name: 'Missing docker dependency'
onStatus: 'FAILED'
logMatches: 'bash: docker: command not found' # regex pattern that matches against the log output of a step
link:
  name: 'Run Docker commands in Bitbucket Pipelines'
  url: 'https://confluence.atlassian.com/x/O1toN'
description: (optional) 'Based on your build errors, this might help:'
```

## Contributing to the diagnoses

We'd love to have your contribution to this project. If you are a vendor and you would like to get involved in the project, please let us know on [pipelines-feedback@atlassian.com](mailto:pipelines-feedback@atlassian.com).
