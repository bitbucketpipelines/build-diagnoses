import sys
import glob
import json
import logging

import warnings
from datetime import datetime
from typing import List, Optional
from dataclasses import dataclass, asdict
from concurrent.futures.thread import ThreadPoolExecutor

import yaml
import cerberus
from colorlog.colorlog import ColoredFormatter

def logger_config():
    """
        Setup the logging environment
    """

    clogger = logging.getLogger()
    format_str = '%(log_color)s%(levelname)s: %(message)s'
    date_format = '%Y-%m-%d %H:%M:%S'
    colors = {'DEBUG': 'green',
              'INFO': 'blue',
              'WARNING': 'bold_yellow',
              'ERROR': 'bold_red',
              'CRITICAL': 'bold_purple'}
    formatter = ColoredFormatter(format_str, date_format, log_colors=colors)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    clogger.addHandler(stream_handler)
    clogger.setLevel(logging.INFO)
    return clogger


logger = logger_config()

LOCAL_METADATA = cerberus.Validator({
    'key': {'type': 'string', 'required': True},
    'name': {'type': 'string', 'required': True},
    'onStatus': {'type': 'string', 'required': True},
    'logMatches': {'type': 'string', 'required': True},
    'links': {'type': 'list', 'required': False},
    'description': {'type': 'string', 'required': False},
})

@dataclass
class Failures:
    criticals: List[str]
    warnings: List[str]
    skipped: List[str]


@dataclass
class ProcessMetadata:
    file: str
    valid: bool
    failures: Failures
    errors: List[str]


@dataclass
class ProcessResult:
    metadata: ProcessMetadata
    data: Optional[dict]


@dataclass
class RuleContext:
    infile: str
    local_yml: dict
    final_yml: dict


class Check:

    def __init__(self, context: RuleContext):
        self.failures = Failures(criticals=[], warnings=[], skipped=[])
        self.context = context

    def get_failures(self):
        return self.failures

def validate(context: RuleContext) -> tuple:
    infile = context.infile

    # RuleEngine
    check = Check(context)

    return check.get_failures()


def extract_information(local_yml: dict, infile: str) -> RuleContext:

    local_yml_valid = LOCAL_METADATA.validate(local_yml)
    if not local_yml_valid:
        warnings.warn("Structure you are using, is deprecated. Please, update to current one", DeprecationWarning)

    final_yml = local_yml.copy()

    return RuleContext(infile=infile, local_yml=local_yml, final_yml=final_yml)


def process(infile: str) -> ProcessResult:
    failures = Failures(criticals=[], warnings=[], skipped = [])
    errors = []
    yaml_file = None

    with open(infile, 'r') as stream:
        try:
            manifest = yaml.safe_load(stream)
            context = extract_information(local_yml=manifest, infile=infile)
            yaml_file = context.final_yml
            failures = validate(context)
        except Exception as e:
            errors.append(str(e))

        print(
            f"Diagnose: {infile}. "
            f"Criticals: {len(failures.criticals)} "
            f"Warnings: {len(failures.warnings)} "
            f"Errors: {len(errors)} "
            f"Skipped: {len(failures.skipped)}"
        )
        return ProcessResult(
            metadata=ProcessMetadata(
                file=infile,
                valid=(len(failures.criticals) == 0 and len(failures.warnings) == 0 and len(errors) == 0),
                failures=failures,
                errors=errors),
            data=yaml_file)


def extract_data(results: List[ProcessResult]) -> List[dict]:
    data: List[dict] = [result.data for result in results if result.data]
    return data


def extract_errors(results: List[ProcessResult]) -> List[ProcessResult]:
    errors: List[ProcessResult] = [x for x in results if not x.metadata.valid]
    return errors


def main():
    path = sys.argv[1] if len(sys.argv) > 1 else 'diagnoses/*.yml'
    list_files = sorted(glob.glob(path))
    logger.info("Starting: " + str(len(list_files)) + " diagnose(s) to verify.")

    t1 = datetime.now()

    with ThreadPoolExecutor() as executor:
        results = list(executor.map(process, list_files))

        errors = extract_errors(results)
        if errors:
            for error in errors:
                if error.metadata.failures.criticals:
                    logger.error(f"{asdict(error.metadata)}")
                else:
                    logger.warning(f"{asdict(error.metadata)}")

        data = extract_data(results)

    with open('diagnoses.json', 'w', encoding='utf8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)

    t2 = datetime.now()
    logger.info("Finished. Time: " + str(t2 - t1))


if __name__ == '__main__':
    main()
